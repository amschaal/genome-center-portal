import django

revoke_sponsor = django.dispatch.Signal(providing_args=["user", "sponsor"])
set_sponsor = django.dispatch.Signal(providing_args=["user", "sponsor"])