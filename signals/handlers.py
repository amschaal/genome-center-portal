from django.dispatch.dispatcher import receiver
from signals import revoke_sponsor, set_sponsor
from gcportal.models import UserProfile, LabPermission
from django_logger.models import Log
from django.db.models.signals import post_save, post_delete
from gcportal.middlewares.ThreadLocal import get_current_user

@receiver(revoke_sponsor, sender=UserProfile)
def log_revoke_sponsor(sender, user, sponsor, **kwargs):
    Log.create(text="%s has lost sponsorship with %s"%(user,sponsor),objects=[user,sponsor])

@receiver(set_sponsor, sender=UserProfile)
def log_set_sponsor(sender, user, sponsor, **kwargs):
    Log.create(text="%s has been sponsored by %s"%(user,sponsor),objects=[user,sponsor])

@receiver(post_save, sender=LabPermission)
def log_new_lab_permission(sender, instance, **kwargs):
    Log.create(text='%s has been given the "%s" permission for %s by %s'%(instance.user,instance.permission,instance.lab,get_current_user()),objects=[instance.user,instance.lab])

@receiver(post_delete, sender=LabPermission)
def log_delete_lab_permission(sender, instance, **kwargs):
    Log.create(text='%s had the "%s" permission revoked for %s by %s'%(instance.user,instance.permission,instance.lab,get_current_user()),objects=[instance.user,instance.lab])

@receiver(post_save, sender=Log)
def log_log(sender, instance, **kwargs):
    #use django logger to output instance.text to log file
    pass
