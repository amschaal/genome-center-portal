from django.contrib import admin
from registration.models import GCRegistration
from import_export.admin import ImportExportActionModelAdmin
from registration.import_export_resources import GCRegistrationResource


class GCRegistrationAdmin(ImportExportActionModelAdmin):
    resource_class = GCRegistrationResource
    search_fields = ('first_name','last_name','email','sponsor__name','username','approved_by__username','approved_by__first_name','approved_by__last_name')
    ordering = ('created',)
#     fields = ('created','first_name','last_name','email','group','username','approved_by','approved')
#     filter_horizontal = ('permissions',)
    list_display = ('created','first_name','last_name','email','sponsor','username','approved_by','approved','approval_message')
admin.site.register(GCRegistration, GCRegistrationAdmin)