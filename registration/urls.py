from django.conf.urls import url
from registration import views
urlpatterns = [
    url(r'^register/$', views.register, name='register'),
    url(r'^register/sponsored/(?P<token>[^/]+)/', views.SponsoredRegistrationView.as_view(), name='sponsored_registration'),
    url(r'^complete/', views.complete_account, name='complete_account'),
    url(r'^confirm_registration/(?P<uuid>[^/]+)/', views.confirm_registration, name='confirm_registration'),
    url(r'^cancel_registration/(?P<uuid>[^/]+)/', views.cancel_registration, name='cancel_registration'),
    url(r'^approve_registration/(?P<uuid>[^/]+)/', views.approve_registration, name='approve_registration'),
    url(r'^deny_registration/(?P<uuid>[^/]+)/', views.deny_registration, name='deny_registration'),
    url(r'^registrations/pending/', views.pending_registrations, name='pending_registrations'),
    url(r'^change_email/confirm/(?P<token>[^/]+)/', views.ConfirmChangeEmail.as_view(), name='confirm_change_email'),
    url(r'^change_email/', views.change_email, name='change_email'),
]
