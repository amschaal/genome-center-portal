# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0004_gcregistration_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='gcregistration',
            old_name='group',
            new_name='sponsor',
        ),
    ]
