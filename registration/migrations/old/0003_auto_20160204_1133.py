# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0002_auto_20160203_1700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gcregistration',
            name='created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='gcregistration',
            name='ucd',
            field=models.BooleanField(default=False, help_text=b'Are you a UC Davis student or employee?', verbose_name=b'UCD'),
        ),
        migrations.AlterField(
            model_name='gcregistration',
            name='username',
            field=models.CharField(help_text=b'Required. 30 characters or fewer. Lowercase letters only.', max_length=30, unique=True, error_messages={b'unique': b'A user with that username already exists.'}, validators=[django.core.validators.RegexValidator(b'^[a-z]+$', b'Enter a valid username. This value may contain only lowercase letters.', b'invalid')]),
        ),
    ]
