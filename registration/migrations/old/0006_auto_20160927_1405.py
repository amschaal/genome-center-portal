# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0005_auto_20160927_1405'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gcregistration',
            name='sponsor',
            field=models.ForeignKey(related_name='registrations', to='auth.Group', help_text=b'Enter group or lab sponsoring the account.  If your group is not listed, please email "sysadmin@genomecenter.ucdavis.edu".'),
        ),
    ]
