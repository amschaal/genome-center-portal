# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0006_auto_20160927_1405'),
    ]

    operations = [
        migrations.AddField(
            model_name='gcregistration',
            name='approval_message',
            field=models.TextField(null=True, blank=True),
        ),
    ]
