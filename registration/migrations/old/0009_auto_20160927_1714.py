# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0008_userprofile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='category',
            field=models.CharField(blank=True, max_length=15, null=True, choices=[(b'undergrad', b'Undergraduate Student'), (b'grad', b'Graduate Student'), (b'postdoc', b'Post Doctorate'), (b'staff', b'Staff'), (b'faculty', b'Faculty'), (b'other', b'Other')]),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='sponsor',
            field=models.ForeignKey(related_name='user_profiles', blank=True, to='auth.Group', help_text=b'Enter group or lab sponsoring the account.  If your group is not listed, please email "sysadmin@genomecenter.ucdavis.edu".', null=True),
        ),
    ]
