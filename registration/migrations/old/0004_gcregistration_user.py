# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('registration', '0003_auto_20160204_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='gcregistration',
            name='user',
            field=models.OneToOneField(related_name='registration', null=True, blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
