# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='GCRegistration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now=True)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('username', models.CharField(help_text=b'Required. 50 characters or fewer. Letters and digits only.', max_length=50, unique=True, error_messages={b'unique': b'A user with that username already exists.'}, validators=[django.core.validators.RegexValidator(b'^[\\w]+$', b'Enter a valid username. This value may contain only letters and numbers.', b'invalid')])),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('confirmation_key', models.UUIDField(default=uuid.uuid4)),
                ('confirmed', models.BooleanField(default=False)),
                ('approval_key', models.UUIDField(default=uuid.uuid4)),
                ('approved', models.DateTimeField(null=True, blank=True)),
                ('approved_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('group', models.ForeignKey(help_text=b'Enter the primary group or lab associated with the account.', to='auth.Group')),
            ],
        ),
    ]
