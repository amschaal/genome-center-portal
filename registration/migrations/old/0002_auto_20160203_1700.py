# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='gcregistration',
            name='category',
            field=models.CharField(default='other', max_length=15, choices=[(b'undergrad', b'Undergraduate Student'), (b'grad', b'Graduate Student'), (b'postdoc', b'Post Doctorate'), (b'staff', b'Staff'), (b'faculty', b'Faculty'), (b'other', b'Other')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='gcregistration',
            name='ucd',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='gcregistration',
            name='group',
            field=models.ForeignKey(related_name='registrations', to='auth.Group', help_text=b'Enter the primary group or lab associated with the account.  If your group is not listed, please email "sysadmin@genomecenter.ucdavis.edu".'),
        ),
        migrations.AlterField(
            model_name='gcregistration',
            name='username',
            field=models.CharField(help_text=b'Required. 50 characters or fewer. Letters only.', max_length=50, unique=True, error_messages={b'unique': b'A user with that username already exists.'}, validators=[django.core.validators.RegexValidator(b'^[\\w]+$', b'Enter a valid username. This value may contain only letters and numbers.', b'invalid')]),
        ),
    ]
