# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('registration', '0007_gcregistration_approval_message'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.CharField(max_length=15, choices=[(b'undergrad', b'Undergraduate Student'), (b'grad', b'Graduate Student'), (b'postdoc', b'Post Doctorate'), (b'staff', b'Staff'), (b'faculty', b'Faculty'), (b'other', b'Other')])),
                ('ucd', models.BooleanField(default=False, help_text=b'Are you a UC Davis student or employee?', verbose_name=b'UCD')),
                ('sponsor', models.ForeignKey(related_name='user_profiles', to='auth.Group', help_text=b'Enter group or lab sponsoring the account.  If your group is not listed, please email "sysadmin@genomecenter.ucdavis.edu".')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
