from import_export import resources, fields
from registration.models import GCRegistration
from import_export.widgets import ForeignKeyWidget
from django.contrib.auth.models import User, Group
from gcportal.models import Lab


class GCRegistrationResource(resources.ModelResource):
    approved_by = fields.Field(column_name='approved_by',attribute='approved_by',
            widget=ForeignKeyWidget(User,'username'))
    sponsor = fields.Field(column_name='sponsor',attribute='sponsor',
            widget=ForeignKeyWidget(Lab,'name'))
#     def dehydrate_approved_by(self, reg):
#         print 'DEHYDRATE REG: '+reg.approved_by.username
#         return reg.approved_by.username
    class Meta:
        model = GCRegistration
        fields = ('username','email','created','sponsor','category','ucd','first_name','last_name','approved_by','approved','approval_message')
