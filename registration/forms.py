from django import forms
from registration.models import GCRegistration
from django.contrib.auth.models import User
from django.contrib.auth.forms import SetPasswordForm
from gcportal.models import UserProfile
from kerberos.admin import KerberosAdmin
from django.conf import settings
from blacklist import usernames as blacklisted_usernames
import subprocess

class GCRegistrationForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(GCRegistrationForm, self).__init__(*args,**kwargs)
        if self.fields.has_key('sponsor'):
            self.fields['sponsor'].required = True 
    class Meta:
        model = GCRegistration
        fields = ('sponsor','category','ucd','first_name','last_name','email','username')
    def clean_username(self):
        username = self.cleaned_data['username']
        #Check database
        if User.objects.filter(username=username).first() or GCRegistration.objects.filter(username=username).first():
            raise forms.ValidationError("This username is already taken.")
        #Check principals
        if getattr(settings,'KRB_ENABLE',False):
            kadmin = KerberosAdmin.create()
            if kadmin.username_exists(username):
                raise forms.ValidationError("That kerberos principal name is already taken.")
        #Check system users
        if subprocess.call(['getent','passwd',username]) != 2:
            raise forms.ValidationError("That user exists on the system.")
        #Check blacklist
        if username in blacklisted_usernames:
            raise forms.ValidationError("That username is blacklisted.")
        
        return username
    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).first() or GCRegistration.objects.filter(email=email).first():
            raise forms.ValidationError("An account with this email already exists.")
        return email

class ExistingGCRegistrationForm(GCRegistrationForm):
    def __init__(self,*args,**kwargs):
        self.user = kwargs.pop('user')
        super(ExistingGCRegistrationForm, self).__init__(*args,**kwargs)
    class Meta:
        model = GCRegistration
        fields = ('sponsor','category','ucd','first_name','last_name','email')
    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exclude(username=self.user.username).first():
            raise forms.ValidationError("An account with this email already exists.")
        return email

class SponsoredGCRegistrationForm(GCRegistrationForm):
    class Meta:
        model = GCRegistration
        fields = ('category','ucd','first_name','last_name','username')

class UserForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(UserForm, self).__init__(*args,**kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
#         self.fields['email'].required = True
    class Meta:
        model = User
        fields = ('first_name','last_name')
        
class UserProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        # magic 
        self.user = kwargs['instance'].user
        user_kwargs = kwargs.copy()
        user_kwargs['instance'] = self.user
        self.uf = UserForm(*args, **user_kwargs)
        # magic end 

        super(UserProfileForm, self).__init__(*args, **kwargs)

        self.fields.update(self.uf.fields)
        self.initial.update(self.uf.initial)

        # define fields order if needed
        self.fields.keyOrder = ( 'first_name','last_name','email', 'category','ucd') #'sponsor',

        self.fields['category'].required = True
    def save(self, *args, **kwargs):
        # save both forms   
        self.uf.save(*args, **kwargs)
        return super(UserProfileForm, self).save(*args, **kwargs)

    class Meta:
        model = UserProfile
        fields = ('category','ucd')

class KerberosSetPasswordForm(SetPasswordForm):
    def clean(self):
        cleaned_data = super(KerberosSetPasswordForm, self).clean()
        new_password = cleaned_data.get("new_password1")
        try:
#             The only way to validate a password with python kadmin is to set it.
            self.user.principal.set_password(new_password)#princ.change_password(password)
        except Exception, e:
            raise forms.ValidationError(e.message['message'])
    def save(self, commit=True):
        #Doesn't do anything, since password setting happens in clean.
#         kadmin = KerberosAdmin.create()
#         kadmin.set_password(self.user,self.cleaned_data['new_password1'])
        return self.user
        
class ChangeEmailForm(forms.Form):
    email = forms.EmailField()
