from django.shortcuts import render, redirect
from registration.forms import GCRegistrationForm, KerberosSetPasswordForm,\
    ChangeEmailForm, ExistingGCRegistrationForm, SponsoredGCRegistrationForm
from registration.models import GCRegistration
from django.utils import timezone
from django.contrib.auth.models import User
# from guardian.decorators import permission_required_or_403
from gcportal.forms import PolicyForm
from gcportal.models import Policy, AcceptedPolicy, LabPermission, Lab,\
    UserProfile
from gcportal.decorators import has_lab_permissions, login_required
from django.contrib.auth.forms import SetPasswordForm
from django.conf import settings
from django_confirm.views import ConfirmationView

def register(request):
    form = GCRegistrationForm()
    policy_form = PolicyForm(prefix='policy_')
    policies = Policy.objects.filter(active=True)
    if request.method == 'POST':
        form = GCRegistrationForm(request.POST)
        policy_form = PolicyForm(request.POST,prefix='policy_')
        if form.is_valid() and policy_form.is_valid():
            registration = form.save()
            user = User.objects.create(first_name=registration.first_name,last_name=registration.last_name,username=registration.username,email=registration.email,is_active=False)
            registration.user = user
            for policy in policies:
                AcceptedPolicy.objects.create(policy=policy,text=policy.text,user=user)
            registration.save()
            registration.email_registrant()
            return render(request, 'registration/submitted.html', {'registration':registration})
    return render(request, 'registration/register.html', {'form':form,'policy_form':policy_form,'policies':policies})

class SponsoredRegistrationView(ConfirmationView):
    action_id = 'sponsored_registration'
#     def get_form_class(self):
#         return KerberosSetPasswordForm if getattr(settings,'KRB_ENABLE',False) else SetPasswordForm
    def post(self,request,*args,**kwargs):
        form = SponsoredGCRegistrationForm(request.POST)
        policy_form = PolicyForm(request.POST,prefix='policy_')
        policies = Policy.objects.filter(active=True)
        if form.is_valid() and policy_form.is_valid():
            registration = form.save(commit=False)
            registration.email = self.token.data['email']
            registration.sponsor = self.token.content_object
            user = User.objects.create(first_name=registration.first_name,last_name=registration.last_name,username=registration.username,email=self.token.data['email'],is_active=True)
            registration.user = user
            for policy in policies:
                AcceptedPolicy.objects.create(policy=policy,text=policy.text,user=user)
            registration.save()
            registration.activate_user()
            for perm in self.token.data.get('permissions',[]):
                LabPermission.objects.create(user=user,lab=self.token.content_object,permission=perm)
            self.token.delete()
            return render(request, 'registration/sponsored_registration.html', {'registration':registration,'user':user})
        return render(request, 'registration/sponsored_registration.html', {'form':form,'policy_form':policy_form,'policies':policies,'sponsor':self.token.content_object})
    def get(self,request,*args,**kwargs):
        form = SponsoredGCRegistrationForm()
        policy_form = PolicyForm(prefix='policy_')
        policies = Policy.objects.filter(active=True)
        return render(request, 'registration/sponsored_registration.html', {'form':form,'policy_form':policy_form,'policies':policies,'sponsor':self.token.content_object,'email':self.token.data['email']})

"""
For Genome Center users who already have a principal, make them fill out a registration form, but don't create a user
"""
@login_required(requires_complete_account=False)
def complete_account(request):
    if GCRegistration.objects.filter(user=request.user).first(): #UserProfile.objects.filter(user=request.user,sponsor__isnull=False).first()
        return redirect('home')
    form = ExistingGCRegistrationForm(user=request.user)
    policy_form = PolicyForm(prefix='policy_')
    policies = Policy.objects.filter(active=True).exclude(accepted_policies__user=request.user)
    if request.method == 'POST':
        form = ExistingGCRegistrationForm(request.POST,user=request.user)
        policy_form = PolicyForm(request.POST,prefix='policy_')
        if form.is_valid() and (policy_form.is_valid() or policies.count() == 0):
            registration = form.save(commit=False)
            registration.user = request.user
            registration.username = request.user.username
            registration.save()
            request.user.first_name=registration.first_name
            request.user.last_name=registration.last_name
            request.user.save()
            if getattr(request.user, 'profile',None):
                profile = UserProfile.objects.filter(user=request.user).update(sponsor=registration.sponsor,category=registration.category,ucd=registration.ucd)
            else:
                profile = UserProfile.objects.create(user=request.user,sponsor=registration.sponsor,category=registration.category,ucd=registration.ucd)
            for policy in policies:
                AcceptedPolicy.objects.create(policy=policy,text=policy.text,user=request.user)
            request.user.profile.send_email_change(registration.email)
            return render(request, 'registration/account_completed.html', {'registration':registration})
    return render(request, 'registration/complete_account.html', {'form':form,'policy_form':policy_form,'policies':policies})

def confirm_registration(request,uuid):
    registration = GCRegistration.objects.get(confirmation_key=uuid)
    if not registration.confirmed:
        registration.confirmed = True
        registration.save()
        registration.email_approvers()
    return render(request, 'registration/confirm_registration.html', {'registration':registration})

# def reset_password(request,uuid):
#     reset = PasswordReset.objects.get(token=uuid)
#     form_class = KerberosSetPasswordForm if getattr(settings,'KRB_ENABLE',False) else SetPasswordForm
#     if request.method == "POST":
#         form = form_class(reset.user,request.POST)
#         if form.is_valid():
#             form.save()
#             reset.delete()
#             return render(request, 'registration/reset_password_done.html', {'form':form})
#     else:
#         form = form_class(reset.user)
#     return render(request, 'registration/reset_password.html', {'form':form})


def cancel_registration(request,uuid):
    registration = GCRegistration.objects.get(confirmation_key=uuid,approved__isnull=True)
    if registration.user:
        registration.user.delete()
    registration.delete()
    return render(request, 'registration/cancel_registration.html', {})

@login_required
# @permission_required_or_403('auth.approve',(Group,'registrations__approval_key','uuid'))
@has_lab_permissions(['approver'],lab_lookup=('registrations__approval_key','uuid'))
def approve_registration(request,uuid):
    registration = GCRegistration.objects.get(approval_key=uuid)
    if not registration.approved_by:
        if request.method == 'GET':
            return render(request, 'registration/approve_registration.html', {'registration':registration})
        elif request.method == 'POST':
            registration.approved_by = request.user
            registration.approved = timezone.now()
            registration.approval_message = request.POST.get('approval_message',None)
            registration.save()
            registration.activate_user()
    return render(request, 'registration/approve_registration.html', {'registration':registration})

@login_required
# @permission_required_or_403('auth.approve',(Group,'registrations__approval_key','uuid'))
@has_lab_permissions(['approver'],lab_lookup=('registrations__approval_key','uuid'))
def deny_registration(request,uuid):
    registration = GCRegistration.objects.filter(approval_key=uuid,approved_by=None).first()
    if registration:
        registration.deny_user()
    return render(request, 'registration/cancel_registration.html', {'registration':registration})

@login_required
@has_lab_permissions(['approver'])
def pending_registrations(request):
    labs = Lab.objects.filter(lab_permissions__user=request.user).filter(lab_permissions__permission=LabPermission.PERMISSION_APPROVER)#get_objects_for_user(request.user,['approve'],klass=Group)
    if request.user.is_superuser:
        registrations = GCRegistration.objects.filter(confirmed=True,approved_by=None).order_by('created')
    else:
        registrations = GCRegistration.objects.filter(confirmed=True,approved_by=None,sponsor__in=labs).order_by('created')
    return render(request, 'registration/pending_registrations.html', {'registrations':registrations})

@login_required(requires_complete_account=False)
def change_email(request):
    if request.method == "POST":
        form = ChangeEmailForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            request.user.profile.send_email_change(email)
            return render(request, 'registration/change_email.html', {'form':form,'new_email':email})
    else:
        form = ChangeEmailForm()
    return render(request, 'registration/change_email.html', {'form':form})

class ConfirmChangeEmail(ConfirmationView):
    action_id = 'change_email'
    def get(self,request,*args,**kwargs):
        user = self.token.content_object
        old_email = user.email
        user.email = self.token.data['email']
        user.save()
        self.token.delete()
        return render(request, 'registration/confirm_change_email.html', {'email':user.email,'old_email':old_email})

class ResetPasswordView(ConfirmationView):
    action_id = 'password_reset'
    def get_form_class(self):
        return KerberosSetPasswordForm if getattr(settings,'KRB_ENABLE',False) else SetPasswordForm
    def post(self,request,*args,**kwargs):
        form = self.get_form_class()(self.token.content_object,request.POST)
        if form.is_valid():
            form.save()
            self.token.delete()
            return render(request, 'registration/reset_password_done.html', {'form':form})
        return render(request, 'registration/reset_password.html', {'form':form})
    def get(self,request,*args,**kwargs):
        form = self.get_form_class()(self.token.content_object)
        return render(request, 'registration/reset_password.html', {'form':form})