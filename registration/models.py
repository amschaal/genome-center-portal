from django.db import models
from django.contrib.auth.models import User
from django.core import validators
import uuid
from django.conf import settings
from gcportal.emails import Mailer
from gcportal.models import LabPermission, Lab, UserProfile
from django.core.urlresolvers import reverse
from kerberos.admin import KerberosAdmin

CATEGORY_CHOICES = (('undergrad','Undergraduate Student'),('grad','Graduate Student'),('postdoc','Post Doctorate'),('staff','Staff'),('faculty','Faculty'),('other','Other'))

# class UserProfile(models.Model):
#     category = models.CharField(max_length=15,choices=CATEGORY_CHOICES)
#     ucd = models.BooleanField('UCD',default=False,help_text="Are you a UC Davis student or employee?")

class GCRegistration(models.Model):
    user = models.OneToOneField(User,null=True,blank=True,related_name='registration',on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    sponsor = models.ForeignKey(Lab,null=True,blank=True,on_delete=models.SET_NULL,help_text='Enter group or lab sponsoring the account.  If your lab is not listed, please email "sysadmin@genomecenter.ucdavis.edu".',related_name='registrations')
    category = models.CharField(max_length=15,choices=(('undergrad','Undergraduate Student'),('grad','Graduate Student'),('postdoc','Post Doctorate'),('staff','Staff'),('faculty','Faculty'),('other','Other')))
    ucd = models.BooleanField('UCD',default=False,help_text="Are you a UC Davis student or employee?")
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    username = models.CharField(max_length=30, unique=True,
        help_text='Required. 30 characters or fewer. Lowercase letters only.',
        validators=[
            validators.RegexValidator(r'^[a-z]+$','Enter a valid username. This value may contain only lowercase letters.', 'invalid'),
        ],
        error_messages={
            'unique': "A user with that username already exists.",
        })
    email = models.EmailField(unique=True)
    confirmation_key = models.UUIDField(default=uuid.uuid4)
    confirmed = models.BooleanField(default=False)
    approval_key = models.UUIDField(default=uuid.uuid4)
    approved_by = models.ForeignKey(User,null=True,blank=True)
    approved = models.DateTimeField(null=True,blank=True)
    approval_message = models.TextField(null=True,blank=True)
    def activate_user(self):
        User.objects.filter(username=self.user.username).update(is_active=True)
        profile = UserProfile.objects.create(user=self.user,sponsor=self.sponsor,category=self.category,ucd=self.ucd)
        token = profile.create_password_reset_token()
        url = settings.BASE_URL+reverse('password_reset', kwargs={'token':token.token})
        if getattr(settings,'KRB_ENABLE',False):
            kadm = KerberosAdmin.create()
            kadm.add_principal(self.user)
        Mailer().send_mail('Your Genome Center account has been approved', to=[self.email], context={'registration':self,'url':url}, message_template='registration/email/account_approved.txt')
    def email_registrant(self):
        Mailer().send_mail('Please confirm your request for a Genome Center account', to=[self.email], context={'registration':self}, message_template='registration/email/confirm_email.txt')
    def deny_user(self):
        Mailer().send_mail('Your Genome Center account request has been denied', to=[self.email], context={'registration':self}, message_template='registration/email/account_denied.txt')
        if self.user:
            self.user.delete()
        self.delete()
    def email_approvers(self):
        users = User.objects.filter(lab_permissions__lab=self.sponsor).filter(lab_permissions__permission=LabPermission.PERMISSION_APPROVER).distinct()#get_users_with_perms(self.group,attach_perms=True)
        print users
        emails = []
        if len(emails) == 0:
            emails = [settings.SYSADMIN_TO_EMAIL]
        for user in users:
            emails.append(user.email)
        Mailer().send_mail('Genome Center account approval requested', to=emails, context={'registration':self}, message_template='registration/email/approve_email.txt')

