from rest_framework import filters
from gcportal.models import LabPermission

class LabPermissionFilter(filters.BaseFilterBackend):
    """
    Filters groups based on permission for that group
    """
    def filter_queryset(self, request, queryset, view):
        permissions = request.query_params.getlist('has_permissions',None)
        any_permission = request.query_params.getlist('has_any_permission',None)
        if not permissions and not any_permission:
            return queryset
        print permissions
        if permissions:
            return LabPermission.get_labs_with_permissions(request.user, permissions, queryset, use_superuser=True,require_all=True).distinct()
        else:
            return LabPermission.get_labs_with_permissions(request.user, any_permission, queryset,use_superuser=True, require_all=False).distinct()
