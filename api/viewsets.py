from rest_framework import viewsets, mixins
from django.contrib.auth.models import User
from api.serializers import LabSerializer, LabPermissionSerializer,\
    UserSerializer, UnixGroupSerializer, PrincipalSerializer
from rest_framework.generics import get_object_or_404
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from gcportal.models import LabPermission, Lab, UnixGroup
from rest_framework.exceptions import PermissionDenied
from api.filters import LabPermissionFilter
from rest_framework import filters
from kerberos.admin import KerberosAdmin
from rest_framework.permissions import IsAdminUser
from django.conf import settings

# class LabViewSet(viewsets.ViewSet):
#     def list(self,request): 
#         shortcuts.get_objects_for_user(request.user, 'admin', klass=Lab)

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UserSerializer
    search_fields = ('username', 'email','first_name','last_name')
    filter_fields = {'profile__sponsor__id':['exact']}
    def __init__(self,*args,**kwargs):
        super(UserViewSet, self).__init__(*args,**kwargs)
        if getattr(settings,'KRB_ENABLE',False):
            self.kerberos_admin = KerberosAdmin.create()
    def get_serializer_context(self):
        context =  viewsets.ReadOnlyModelViewSet.get_serializer_context(self)
        if self.request.query_params.has_key('include_principal') and hasattr(self, 'kerberos_admin'):
            context['kerberos_admin'] = self.kerberos_admin
        return context
    def get_queryset(self):
        return User.objects.filter(id__gte=1)
    @detail_route(methods=['post'])
    def revoke_sponsorship(self, request, pk):
        instance = self.get_object()
        if not instance.profile.sponsor:
            raise Exception('User does not currently have a sponsor')
        if not LabPermission.has_permission(instance.profile.sponsor_id, request.user, LabPermission.PERMISSION_ADMIN):
            raise PermissionDenied
        instance.profile.revoke_sponsor(revoked_by=request.user)
        return Response(UserSerializer(instance).data)
class LabViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = LabSerializer
    search_fields = ('name',)
    filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,LabPermissionFilter,)
    def get_queryset(self):
        return Lab.objects.all()
        #shortcuts.get_objects_for_user(self.request.user, 'admin', klass=Lab)
    @detail_route(methods=['get'])
    def permissions(self, request, pk):
        if not LabPermission.has_permission(lab_id=pk,user=request.user,permission=LabPermission.PERMISSION_ADMIN):
            raise PermissionDenied
        lab = self.get_object()
        return Response(LabPermissionSerializer(lab).data)
    @detail_route(methods=['post'])
    def set_permissions(self, request, pk):
        if not LabPermission.has_permission(lab_id=pk,user=request.user,permission=LabPermission.PERMISSION_ADMIN):
            raise PermissionDenied
        lab = self.get_object()
        LabPermission.objects.filter(lab=lab).delete()
        for up in request.data.get('user_permissions',[]):
            for p in up['permissions']:
                LabPermission.objects.get_or_create(user_id=up['user']['id'],lab=lab,permission=p)
        return Response(LabPermissionSerializer(lab).data)
    @detail_route(methods=['post'])
    def add_permission(self, request, pk):
        if not LabPermission.has_permission(lab_id=pk,user=request.user,permission=LabPermission.PERMISSION_ADMIN):
            raise PermissionDenied
        user_id = request.data['user_id']
        permission = request.data['permission']
        lab = self.get_object()
        instance, created = LabPermission.objects.get_or_create(lab=lab,user_id=user_id,permission=permission)
        return Response({'status':'success','message':'user permission added'})
    @detail_route(methods=['post'])
    def remove_permission(self, request, pk):
        if not LabPermission.has_permission(lab_id=pk,user=request.user,permission=LabPermission.PERMISSION_ADMIN):
            raise PermissionDenied
        user_id = request.data['user_id']
        permission = request.data['permission']
        lab = self.get_object()
        LabPermission.objects.filter(lab=lab,user_id=user_id,permission=permission).delete()
        return Response({'status':'success','message':'user permission removed'})
    class Meta:
        model = Lab

class GroupViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UnixGroupSerializer
    filter_fields = {'lab__id':['exact']}
    def get_queryset(self):
        return UnixGroup.objects.all()

class PrincipalViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,viewsets.GenericViewSet):
    serializer_class = PrincipalSerializer
    _paginator = None
    permission_classes = (IsAdminUser,)
    def __init__(self,*args,**kwargs):
        super(PrincipalViewSet, self).__init__(*args,**kwargs)
        self.kadmin = KerberosAdmin.create().get_kadmin()
    def get_queryset(self):
        #can use self.request to filter
        return [self.kadmin.getprinc(p) for p in self.kadmin.principals()]
    def get_object(self):
        return self.kadmin.getprinc(self.kwargs['pk'])
    
    
