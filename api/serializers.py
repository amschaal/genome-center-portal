from rest_framework import serializers
from django.contrib.auth.models import User
from gcportal.models import LabPermission,UserProfile, Lab, UnixGroup
from django.db.models.query import Prefetch


class LabSerializer(serializers.ModelSerializer):
    name = serializers.CharField(read_only=True)
    class Meta:
        model = Lab
        exclude = ()

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        exclude = ()
    
class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer()
    class Meta:
        model = User
        fields = ('id','username','first_name','last_name','email','profile')
    def __init__(self,*args,**kwargs):
        self.kerberos_admin = kwargs.get('context',{}).pop('kerberos_admin',None)
        super(UserSerializer, self).__init__(*args,**kwargs)
    def to_representation(self, instance):
        data = serializers.ModelSerializer.to_representation(self, instance)
        if self.kerberos_admin:
            data['principal'] = instance.principal.serialize()
        return data
class LabPermissionSerializer(serializers.BaseSerializer):
#     def to_internal_value(self, data):
#         lab_permissions
#         raise NotImplementedError('`to_internal_value()` must be implemented.')
    
    def to_representation_old(self, lab):
        users = User.objects.filter(lab_permissions__lab=lab).prefetch_related(
                    Prefetch('lab_permissions',queryset=LabPermission.objects.filter(lab=lab), to_attr='permissions')
                ).distinct()
        permissions = []
        for user in users:
            permissions.append({'user':UserSerializer(user).data,'permissions':[gp.permission for gp in user.permissions]})
        serialized = LabSerializer(lab).data
        serialized['user_permissions']=permissions
        return serialized
    def to_representation(self, lab):
#         users = User.objects.filter(lab_permissions__lab=lab).prefetch_related(
#                     Prefetch('lab_permissions',queryset=LabPermission.objects.filter(lab=lab), to_attr='permissions')
#                 ).distinct()
        lab_permissions = LabPermission.objects.filter(lab=lab).select_related('user')
        permissions = {perm[0]:[] for perm in LabPermission.GROUP_PERMISSIONS}
        for perm in lab_permissions:
            permissions[perm.permission].append(UserSerializer(perm.user).data)
        serialized = LabSerializer(lab).data
        serialized['permissions']=permissions
        return serialized

class UnixGroupSerializer(serializers.ModelSerializer):
    users = serializers.SerializerMethodField()
    def get_users(self,object):
#         return object.users()
        return [UserSerializer(u).data for u in object.user_queryset()]
        
    class Meta:
        model = UnixGroup
        exclude = ()

class PrincipalSerializer(serializers.BaseSerializer):
    def to_representation(self, obj):
        return {
            'principal': obj.principal,
            'name': obj.name,
#             'mod_name': obj.mod_name,
            'mod_date': obj.mod_date,
            'last_pwd_change': obj.last_pwd_change,
            'last_success': obj.last_success,
            'last_failure': obj.last_failure,
            'expire': obj.expire,
            'pwexpire': obj.pwexpire,
            'maxlife': obj.maxlife,
            'maxrenewlife': obj.maxrenewlife,
            'policy': obj.policy,
            'kvno': obj.kvno
        }
            
            
    
