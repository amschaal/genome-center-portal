from django.conf.urls import  url
from rest_framework.routers import DefaultRouter
from api.viewsets import LabViewSet, UserViewSet, GroupViewSet, PrincipalViewSet
from django_logger.api.views import LogViewset

router = DefaultRouter()
router.register(r'labs', LabViewSet, 'Lab')
router.register(r'users', UserViewSet, 'User')
router.register(r'unix_groups', GroupViewSet, 'UnixGroup')
router.register(r'principals', PrincipalViewSet, 'Principal')
router.register(r'logs', LogViewset, 'Log')


urlpatterns = router.urls

# urlpatterns = [
#     url(r'^', 'ezreg.payment.touchnet.views.postback',name='touchnet_postback'),
# ]
