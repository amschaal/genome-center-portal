from __future__ import unicode_literals

from django.apps import AppConfig


class DjangoConfirmConfig(AppConfig):
    name = 'django_confirm'
