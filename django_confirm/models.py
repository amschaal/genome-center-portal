from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.conf import settings
import uuid
from django.urls.base import reverse
import jsonfield

class Token(models.Model):
    token = models.CharField(max_length=50,unique=True)
    action = models.CharField(max_length=30,null=True,blank=True)
    confirmed_by = models.ForeignKey(User,null=True,blank=True)
    confirmed = models.DateTimeField(null=True,blank=True)
    expires = models.DateTimeField(null=True,blank=True)
    content_type = models.ForeignKey(ContentType, null=True,blank=True, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(null=True,blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    data = jsonfield.JSONField()
    @staticmethod
    def generate_token():
        return uuid.uuid4()
    @staticmethod
    def create(**kwargs):
        kwargs['token']=Token.generate_token()
        return Token.objects.create(**kwargs)
    def url(self, view_name=None):
        return settings.BASE_URL+reverse(view_name or self.action, kwargs={'token':self.token})
