from django.shortcuts import render
from django.views.generic.base import View
from django.http.response import JsonResponse
from django.core.exceptions import PermissionDenied
import datetime
from django_confirm.models import Token


# url(r'^confirm/(?P<token>[^/]+)/', ConfirmationView.as_view(), name='confirm_change_email'),
class ConfirmationView(View):
#     def dispatch(self, request, *args, **kwargs):
#         return View.dispatch(self, request, *args, **kwargs)
    permission_classes = []
    action_id = None
#     def action(self,request,token,*args,**kwargs):
    def get_token(self,request,*args,**kwargs):
        token = kwargs.get('token',None)
        if not token or token == '':
            raise Exception('Invalid token')
        self.token = Token.objects.get(token=token,action=self.action_id,expires__gte=datetime.datetime.now())
        if not self.token:
            raise Exception('Invalid token')
        return self.token
    def confirm(self,request,*args,**kwargs):
        self.token.confirmed_by = request.user
        self.token.confirmed = datetime.datetime.now()
        self.token.save()
    def dispatch(self, request, *args, **kwargs):
        self.get_token(self,request,*args,**kwargs)
        for permission_class in self.permission_classes:
            if permission_class(request,self.token,*args,**kwargs) == False:
                raise PermissionDenied()
        return View.dispatch(self, request, *args, **kwargs)
#     def get(self, request, *args, **kwargs):
#         
#         return self.action(request, self.token, *args, **kwargs)

