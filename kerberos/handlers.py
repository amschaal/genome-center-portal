from django.dispatch.dispatcher import receiver
from signals import revoke_sponsor, set_sponsor
from gcportal.models import UserProfile

@receiver(revoke_sponsor, sender=UserProfile)
def expire_principal(sender, user, sponsor, **kwargs):
    user.principal.set_expiration()
    
@receiver(set_sponsor, sender=UserProfile)
def unexpire_principal(sender, user, sponsor, **kwargs):
    user.principal.set_expiration(date=user.profile.get_expiration_date())