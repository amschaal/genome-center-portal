from django.conf import settings
from datetime import datetime


class KerberosAdmin(object):
    instance = None
    @staticmethod
    def create():
        import kadmin
        if not KerberosAdmin.instance:
            KerberosAdmin.instance = KerberosAdmin()
            principal = "%s@%s"%(settings.KRB_PRINCIPAL,settings.KRB_REALM)
            KerberosAdmin.instance.kadmin = kadmin.init_with_keytab(principal, settings.KRB_KEYTAB)
        return KerberosAdmin.instance
    def get_kadmin(self):
        return self.instance.kadmin
    @staticmethod
    def fully_qualified_principal(name):
        if '@' in name:
            return name
        else:
            return "%s@%s" % (name,settings.KRB_REALM)
    def add_principal(self,user,password=None):
        principal = "%s@%s" % (user.username,settings.KRB_REALM)
        self.instance.kadmin.addprinc(principal,password)
        princ = self.instance.kadmin.getprinc(principal)
        princ.policy = getattr(settings, 'KRB_DEFAULT_POLICY',None)
        princ.commit()
    
    def get_user_principal(self,user):
        principal = "%s@%s" % (user.username,settings.KRB_REALM)
        return self.instance.kadmin.getprinc(principal)
    def get_principal(self,name):
        principal = KerberosAdmin.fully_qualified_principal(name)
        return self.instance.kadmin.getprinc(principal)
    def username_exists(self,username):
        principal = "%s@%s" % (username,settings.KRB_REALM)
        return self.instance.kadmin.getprinc(principal) is not None

class Principal(object):
    def __init__(self,principal):
        if isinstance(principal, basestring):
            self.princ = KerberosAdmin().create().get_principal(principal)
        else:
            self.princ = principal
    def is_active(self):
        return (not self.princ.expire or self.princ.expire > datetime.now())
    def password_expired(self):
        return self.princ.pwexpire and self.princ.pwexpire < datetime.now()
    def set_password(self,password):
        self.princ.change_password(password)
    def set_expiration(self,date='NOW'):
        if date == 'NOW':
            date = datetime.now()
        self.princ.expire = date
        self.princ.commit()
    def serialize(self):
        from api.serializers import PrincipalSerializer
        return PrincipalSerializer(self.princ).data
