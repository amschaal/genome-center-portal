from django.core.management.base import BaseCommand
import os
from registration.models import GCRegistration
from django.utils import timezone
import datetime

class Command(BaseCommand):
    help = 'Expire any old registrations'

    def handle(self, *args, **options):
        self.stdout.write('Checking share paths...')
        expired = timezone.now() - datetime.timedelta(minutes=4)
        GCRegistration.objects.filter(created__lte=expired)
