from functools import wraps
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.shortcuts import redirect
from gcportal.models import Lab, LabPermission
from registration.models import GCRegistration
from django.utils.decorators import available_attrs
# @has_group_permissions(['admin',...],group_lookup=('id','id'))
def has_lab_permissions(perms,use_superuser=True,lab_lookup=None):
    def perm_decorator(func):
        @wraps(func)
        def func_wrapper(request,*args,**kwargs):
            if not request.user.is_authenticated():
                return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
            if not request.user.is_superuser or not use_superuser:
#                 group = None
                filter_kwargs = {'user':request.user}
                if lab_lookup:
                    lab = Lab.objects.get(**{lab_lookup[0]:kwargs[lab_lookup[1]]})
                    filter_kwargs['lab']= lab
                for perm in perms:
                    if not LabPermission.objects.filter(permission=perm,**filter_kwargs).first():
                        raise PermissionDenied
#             raise Exception('doh!')
            return func(request,*args,**kwargs)
        return func_wrapper
    return perm_decorator


def _login_required(requires_complete_account=True):
    def login_decorator(func):
        @wraps(func,assigned=available_attrs(func))
        def func_wrapper(request,*args,**kwargs):
            if not request.user.is_authenticated():
                return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
            if requires_complete_account:
                if not GCRegistration.objects.filter(user=request.user).first():
                    return redirect('complete_account')
                if not request.user.email:
                    return redirect('profile')
            return func(request,*args,**kwargs)
        return func_wrapper
    return login_decorator
def login_required(function=None, requires_complete_account=True):
    print 'log in required'
    print function
    print requires_complete_account
    actual_decorator = _login_required(requires_complete_account)
    if function:
        return actual_decorator(function)
    return actual_decorator

