var transformDjangoRestResponse = function(data, headers){
	try {
        var jsonObject = JSON.parse(data); // verify that json is valid
        return jsonObject.results ? jsonObject.results : jsonObject;
    }
    catch (e) {
        console.log("did not receive a valid Json: " + e)
    }
    return {};
}

angular.module('portal.models', ['ngResource'])

.factory('Lab', ['$resource', function ($resource) {
  return $resource('/api/labs/:id/', {id:'@id'}, {
    query: { method: 'GET', isArray:true, transformResponse:transformDjangoRestResponse }, 
//    save : { method : 'PUT' },
//    patch : { method : 'PATCH' },
//    create : { method : 'POST' },
//    remove : { method : 'DELETE' }
    get_permissions: { method: 'GET', isArray:false, url: '/api/labs/:id/permissions/'},
    set_permissions: { method: 'POST', isArray:false, url: '/api/labs/:id/set_permissions/' },
    add_permission: { method: 'POST', isArray:false, url: '/api/labs/:id/add_permission/' },
    remove_permission: { method: 'POST', isArray:false, url: '/api/labs/:id/remove_permission/' },
  });
}])
.factory('User', ['$resource', function ($resource) {
  return $resource('/api/users/:id/', {id:'@id'}, {
    query: { method: 'GET', isArray:true, transformResponse:transformDjangoRestResponse },
  	revoke_sponsorship: { method: 'POST', isArray:false, url: '/api/users/:id/revoke_sponsorship/' },
  });
}])
.factory('UnixGroup', ['$resource', function ($resource) {
  return $resource('/api/unix_groups/:id/', {id:'@id'}, {
    query: { method: 'GET', isArray:true, transformResponse:transformDjangoRestResponse },
  });
}]);
