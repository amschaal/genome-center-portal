An request has been made by the following user to request their password:

Name: {{user.first_name}} {{user.last_name}}
Email: {{user.email}}

To APPROVE the password reset request, please visit the following URL:
{{token.url}}?action=approve

To DENY the password reset request, please visit the following URL:
{{token.url}}?action=deny

Thank you,

The Genome Center IT Staff