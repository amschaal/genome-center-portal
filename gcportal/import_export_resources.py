from import_export import resources, fields
from gcportal.models import Machine
from import_export.widgets import ForeignKeyWidget
from django.contrib.auth.models import User

class MachineResource(resources.ModelResource):
    approved_by = fields.Field(column_name='user',attribute='user',
            widget=ForeignKeyWidget(User,'username'))
    class Meta:
        model = Machine


