DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gcportal',
        'USER': 'username',
        'PASSWORD': 'password',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

SYSTEM_FROM_EMAIL = 'no-reply@genomecenter.ucdavis.edu'
SYSADMIN_FROM_EMAIL = 'sysadmin@genomecenter.ucdavis.edu'
SYSADMIN_TO_EMAIL = 'amschaal@ucdavis.edu'
REQUEST_TO_EMAIL = 'helpdesk@genomecenter.ucdavis.edu'

BASE_URL = 'http://localhost:8004'

KRB_ENABLE = True
KRB_REALM = 'GENOMECENTER.UCDAVIS.EDU'
KRB_PRINCIPAL = 'admin'
KRB_KEYTAB = '/path/to/file.keytab'