"""gcportal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from gcportal import views
from api import urls as api_urls
from registration.views import ResetPasswordView


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index,name='index'),
    url(r'^home/$', views.home,name='home'),
    url(r'^software/$', views.software,name='software'),
    url(r'^machines/register/$', views.register_machine,name='register_machine'),
    url(r'^machines/$', views.machines,name='machines'),
    url(r'^machines/(?P<pk>\d+)/admin/$', views.admin_machine,name='admin_machine'),
    url(r'^machines/(?P<pk>\d+)/confirm/$', views.confirm_machine,name='confirm_machine'),
    url(r'^machines/(?P<pk>\d+)/retire/$', views.retire_machine,name='retire_machine'),
    url(r'^machines/(?P<pk>\d+)/delete/$', views.delete_machine,name='delete_machine'),
    url(r'^policies/$', views.policies,name='policies'),
    url(r'^profile/$', views.profile,name='profile'),
    url(r'^policies/(?P<pk>\d+)/$', views.policy,name='policy'),
    url(r'^request_forms/$', views.request_forms,name='request_forms'),
    url(r'^process_form/(?P<form_id>\w+)/$', views.process_form,name='process_form'),
    url(r'^labs/manage/$', views.manage_labs,name='manage_labs'),
    url(r'^labs/manage_groups/$', views.manage_unix_groups,name='manage_unix_groups'),
    url(r'^labs/create/$', views.create_lab,name='create_lab'),
    url(r'^users/manage/$', views.manage_users,name='manage_users'),
    url(r'^users/manage/(?P<username>\w+)/$', views.user_details,name='user_details'),
    url(r'^users/manage/(?P<username>\w+)/modify_principal/$', views.modify_principal,name='modify_principal'),
    url(r'^registration/', include('registration.urls')),
    url(r'^api/', include(api_urls)),
    url(r'^accounts/login/$', auth_views.login, name="login",kwargs={'redirect_authenticated_user': True}),
    url(r'^accounts/logout/$', auth_views.logout, name="logout"),
    url(r'^password_reset/(?P<token>[^/]+)/', ResetPasswordView.as_view(), name='password_reset'),
    url(r'^sponsorship/request_sponsor/$', views.request_new_sponsor,name='request_sponsor'),
    url(r'^sponsorship/confirm/(?P<token>[^/]+)/', views.ConfirmSponsor.as_view(), name='confirm_sponsor'),
    url(r'^account/request_password_reset/$', views.request_new_password,name='request_new_password'),
    url(r'^account/password_reset/confirm/(?P<token>[^/]+)/', views.ConfirmPasswordReset.as_view(), name='confirm_password_reset'),
    
    
    
    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
