from django.contrib import admin

from import_export.admin import ImportExportActionModelAdmin
from gcportal.import_export_resources import MachineResource
from gcportal.models import Machine, Policy, Lab, UnixGroup, UserProfile

# class GroupAdmin(GuardedModelAdmin):
#     search_fields = ('name',)
#     ordering = ('name',)
#     filter_horizontal = ('permissions',)
#     def formfield_for_manytomany(self, db_field, request=None, **kwargs):
#         if db_field.name == 'permissions':
#             qs = kwargs.get('queryset', db_field.rel.to.objects)
#             # Avoid a major performance hit resolving permission names which
#             # triggers a content_type load:
#             kwargs['queryset'] = qs.select_related('content_type')
#         return super(GroupAdmin, self).formfield_for_manytomany(
#             db_field, request=request, **kwargs)


#     created = models.DateTimeField(auto_now_add=True)
#     user = models.ForeignKey(User)
#     type = models.CharField(max_length=10,choices=TYPES)
# #     operating_system = models.CharField()
#     mac_address = models.CharField(max_length=20,
#         validators=[
#             validators.RegexValidator(r'^([0-9a-fA-F]{2}([:-]?|$)){6}$','Please enter a valid Mac address.', 'invalid'),
#         ],unique=True)#r'^([0-9a-fA-F]{2}([:-]?|$)){6}$'
#     description = models.TextField()
#     retired = models.BooleanField(default=False)
#     entered = models.DateTimeField(null=True,blank=True)
#     entered_by = models.ForeignKey(User,null=True,blank=True,related_name="+")


class MachineAdmin(ImportExportActionModelAdmin):
    resource_class = MachineResource
#     search_fields = ('first_name','last_name','email','group__name','username','approved_by__username','approved_by__first_name','approved_by__last_name')
#     ordering = ('created',)
    list_display = ('created','user','type','mac_address','retired','entered','entered_by')
admin.site.register(Machine, MachineAdmin)

class PolicyAdmin(admin.ModelAdmin):
    list_display = ('created','modified','title','required','active')
    readonly_fields = ('modified','created')
admin.site.register(Policy, PolicyAdmin)
admin.site.register(Lab)
admin.site.register(UserProfile)
admin.site.register(UnixGroup)



# admin.site.unregister(Group)
# admin.site.register(Group, GroupAdmin)