from django.conf import settings
from gcportal.models import LabPermission

def settings_context(request):
    return {'BASE_URL':settings.BASE_URL}

def permissions_context(request):
    
    if request.user.is_superuser:
        return {'has_lab_permissions':{LabPermission.PERMISSION_APPROVER:True,LabPermission.PERMISSION_ADMIN:True}}
    elif request.user.is_authenticated():
        return {'has_lab_permissions':{
                                         LabPermission.PERMISSION_APPROVER:LabPermission.get_labs_with_permissions(request.user, [LabPermission.PERMISSION_APPROVER]).count() > 0,
                                         LabPermission.PERMISSION_ADMIN:LabPermission.get_labs_with_permissions(request.user, [LabPermission.PERMISSION_ADMIN]).count() > 0
                                         }
                }
    else:
        return {'has_lab_permissions':{LabPermission.PERMISSION_APPROVER:False,LabPermission.PERMISSION_ADMIN:False}}