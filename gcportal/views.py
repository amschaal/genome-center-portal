from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import user_passes_test
from gcportal.forms import MachineForm, PolicyForm, RequestSponsorForm,\
    RequestPasswordForm, LabForm, LabPIForm, PrincipalForm
from gcportal.models import Machine, Policy, AcceptedPolicy, LabPermission, Lab, UserProfile
from django.utils import timezone
from registration.models import GCRegistration
from gcportal.decorators import has_lab_permissions, login_required
from registration.forms import UserProfileForm
import datetime
from django_confirm.models import Token
from django.contrib.auth.models import User
from django_confirm.views import ConfirmationView
from django.core.exceptions import PermissionDenied
from django.conf import settings
from gcportal.emails import Mailer
from django.core.paginator import Paginator
from django.db.models.query_utils import Q
import operator
import json
from django.contrib.contenttypes.models import ContentType

def index(request):
    return render(request, 'portal/index.html')

def software(request):
    return render(request, 'portal/software.html')

@login_required
def home(request):
    labs = Lab.objects.filter(lab_permissions__user=request.user,lab_permissions__permission=LabPermission.PERMISSION_APPROVER).distinct()#get_objects_for_user(request.user,['approve'],klass=Group)
    registrations = GCRegistration.objects.filter(confirmed=True,approved_by=None,sponsor__in=labs).order_by('created')
    accepted = AcceptedPolicy.objects.filter(user=request.user)
    policies = Policy.objects.filter(active=True).exclude(id__in=[a.policy_id for a in accepted])
    return render(request, 'portal/home.html', {'registrations':registrations,'policies':policies})

@login_required(requires_complete_account=False)
def profile(request):
    profile, created = UserProfile.objects.get_or_create(user=request.user)
    form = UserProfileForm(instance=profile)
    principal = request.user.principal
    if request.method == 'POST':
        form = UserProfileForm(request.POST,instance=profile)
        if form.is_valid():
            form.save()
            return render(request,'portal/profile.html',{'form':form,'principal':principal,'message':'Your profile has been updated.'})
    return render(request,'portal/profile.html',{'form':form,'principal':principal})

@login_required
def register_machine(request):
    form = MachineForm()
    if request.method == 'POST':
        form = MachineForm(request.POST)
        if form.is_valid():
            machine = form.save(commit=False)
            machine.user = request.user
            machine.save()
            machine.email_request()
            return redirect('machines')
    return render(request, 'portal/register_machine.html', {'form':form})

@login_required
def machines(request):
    machines = Machine.objects.filter(user=request.user).order_by('-created')
    return render(request, 'portal/machines.html', {'machines':machines})
@login_required
def delete_machine(request,pk):
    machines = Machine.objects.filter(user=request.user).order_by('-created')
    try:
        Machine.objects.filter(user=request.user,pk=pk,entered__isnull=True).delete()
        message = 'Machine deleted'
    except Exception, e:
        message = e.message
    return render(request, 'portal/machines.html', {'machines':machines,'message':message}) 
@login_required
def retire_machine(request,pk):
    machines = Machine.objects.filter(user=request.user).order_by('-created')
    try:
        machine = Machine.objects.get(user=request.user,pk=pk,entered__isnull=False)
        machine.retired = timezone.now()
        machine.save()
        machine.email_removal_request()
        message = 'A request to remove machine from the network has been sent to the systems administrators.'
    except Exception, e:
        message = e.message
    return render(request, 'portal/machines.html', {'machines':machines,'message':message})    

@login_required
@user_passes_test(lambda u: u.is_superuser)
def confirm_machine(request,pk):
    machine = get_object_or_404(Machine,pk=pk)
    machine.entered = timezone.now()
    machine.entered_by = request.user
    machine.save()
    machine.send_approval_email()
    return render(request, 'portal/machine.html', {'machine':machine})    

@login_required
@user_passes_test(lambda u: u.is_superuser)
def admin_machine(request,pk):
    machine = get_object_or_404(Machine,pk=pk)
    return render(request, 'portal/machine.html', {'machine':machine})

@login_required
def policies(request):
    accepted = AcceptedPolicy.objects.filter(user=request.user)
    policies = Policy.objects.filter(active=True).exclude(id__in=[a.policy_id for a in accepted])
    return render(request, 'portal/policies.html', {'policies':policies,'accepted':accepted})

@login_required
def policy(request,pk):
    next = request.POST.get('next',None) if request.method == 'POST' else request.GET.get('next',None)
    policy = Policy.objects.get(pk=pk)
    accepted = AcceptedPolicy.objects.filter(policy=policy,user=request.user).first()
    if accepted:
        return render(request, 'portal/policy_accepted.html', {'accepted':accepted})
    form = PolicyForm()
    if request.method == 'POST':
        form = PolicyForm(request.POST)
        if form.is_valid():
            accepted = AcceptedPolicy.objects.create(policy=policy,text=policy.text,user=request.user)
            if next:
                return redirect(next)
            return render(request, 'portal/policy_accepted.html', {'accepted':accepted})
    return render(request, 'portal/policy.html', {'form':form,'policy':policy,'next':next})

@login_required
@has_lab_permissions(['admin'])
def manage_labs(request):
#     group = Group.objects.get(id=id)
    permissions = {LabPermission.PERMISSION_APPROVER:'Can approve sponsorship'}
    if request.user.is_superuser:
        permissions[LabPermission.PERMISSION_ADMIN]='Can administer group'
    content_type = ContentType.objects.get_for_model(Lab)
    return render(request, 'portal/manage_labs.html', {'permissions':json.dumps(permissions),'content_type':content_type})

@login_required
@has_lab_permissions(['admin'])
def manage_unix_groups(request):
#     group = Group.objects.get(id=id)
    return render(request, 'portal/manage_unix_groups.html', {})

def request_new_sponsor(request):
    if request.method == 'POST':
        form = RequestSponsorForm(request.POST)
        if form.is_valid():
            user = User.objects.get(username=form.cleaned_data['username'])
            lab = form.cleaned_data['lab']
            expires = datetime.datetime.now() + datetime.timedelta(days=1)
            token = Token.create(action='confirm_sponsor',content_object=user,data={'lab_id':lab.id},expires=expires)
            approvers = User.objects.filter(lab_permissions__lab=lab).filter(lab_permissions__permission=LabPermission.PERMISSION_APPROVER).distinct()#get_users_with_perms(self.group,attach_perms=True)
            emails = []
            for user in approvers:
                emails.append(user.email)
            if len(emails) == 0:
                emails = [settings.SYSADMIN_TO_EMAIL]
            Mailer().send_mail('Genome Center account sponsorship request', to=emails, context={'user':user,'token':token}, message_template='portal/emails/confirm_new_sponsor.txt')
            return render(request, 'portal/change_sponsor.html', {'lab':form.cleaned_data['lab']})
    else:
        form = RequestSponsorForm()
    return render(request, 'portal/change_sponsor.html', {'form':form})

class ConfirmSponsor(ConfirmationView):
    action_id = 'confirm_sponsor'
    def get(self,request,*args,**kwargs):
        action = request.GET.get('action','deny')
        user = self.token.content_object
        lab = Lab.objects.get(id=self.token.data['lab_id'])
        if action == 'approve': 
    #         if not LabPermission.has_permission(lab.id, request.user, LabPermission.PERMISSION_APPROVER):
    #             raise PermissionDenied()
            user.profile.set_sponsor(lab)
        else:
            pass #email user
        self.token.delete()
        return render(request, 'portal/confirm_change_sponsor.html', {'user':user,'lab':lab,'approved':action=='approve'})

def request_new_password(request):
    if request.method == 'POST':
        form = RequestPasswordForm(request.POST)
        if form.is_valid():
            user = User.objects.get(username=form.cleaned_data['username'])
            expires = datetime.datetime.now() + datetime.timedelta(days=1)
            token = Token.create(action='confirm_password_reset',content_object=user,data={'lab_id':user.profile.sponsor.id},expires=expires)
            approvers = User.objects.filter(lab_permissions__lab=user.profile.sponsor).filter(lab_permissions__permission=LabPermission.PERMISSION_APPROVER).distinct()#get_users_with_perms(self.group,attach_perms=True)
            emails = []
            for approver in approvers:
                emails.append(approver.email)
            if len(emails) == 0:
                emails = [settings.SYSADMIN_TO_EMAIL]
            Mailer().send_mail('%s has requested a password change'%str(user), to=emails, context={'user':user,'token':token}, message_template='portal/emails/approve_password_reset.txt')
            return render(request, 'portal/request_password.html', {'lab':user.profile.sponsor})
    else:
        form = RequestPasswordForm()
    return render(request, 'portal/request_password.html', {'form':form})
class ConfirmPasswordReset(ConfirmationView):
    action_id = 'confirm_password_reset'
    def get(self,request,*args,**kwargs):
        action = request.GET.get('action','deny')
        user = self.token.content_object
        lab = Lab.objects.get(id=self.token.data['lab_id'])
        if action == 'approve': 
            user.profile.send_password_reset()
        else:
            pass #email user
        self.token.delete()
        return render(request, 'portal/approve_password_reset.html', {'user':user,'lab':lab,'approved':action=='approve'})

@login_required
def request_forms(request):
    from gcportal.request_forms import request_forms
#     forms_dict = dict([(name, cls) for name, cls in request_forms.__dict__.items() if isinstance(cls, forms.Form)])
#     forms_dict = get_forms()
    return render(request, 'portal/request_forms.html',{'forms':request_forms})

@login_required
def process_form(request, form_id):
    from gcportal.request_forms import request_forms
    form_class = request_forms.get(form_id,None)
    if request.method == 'POST':
        form = form_class(request.POST,user=request.user)
        if form.is_valid():
            message = form.process()
            return render(request, 'portal/process_form.html',{'message':message})
    else:
        form = form_class(user=request.user)
    return render(request, 'portal/process_form.html',{'form':form})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def create_lab(request):
    if request.method == 'POST':
        form = LabPIForm(request.POST)
        if form.is_valid():
            lab = form.save()
            user = form.user
            if not user:
                if form.principal_exists:
                    user = User.objects.create(username=form.cleaned_data['username'],email=form.cleaned_data['email'])
                else:
                    #Create account creation token
                    expires = datetime.datetime.now() + datetime.timedelta(days=3)
                    token = Token.create(action='sponsored_registration',content_object=lab,data={'email':form.cleaned_data['email'],'permissions':[permission[0] for permission in LabPermission.GROUP_PERMISSIONS]},expires=expires)
                    Mailer().send_mail('Create your Genome Center account', to=[form.cleaned_data['email']], context={'lab':lab,'token':token}, message_template='portal/emails/sponsored_registration.txt')
                    return render(request, 'portal/admin/create_lab.html',{'lab':lab,'email':form.cleaned_data['email']})
            if user:
                for perm, label in LabPermission.GROUP_PERMISSIONS:
                    LabPermission.objects.create(user=user,lab=lab,permission=perm)
            return render(request, 'portal/admin/create_lab.html',{'lab':lab,'pi':user})
    else:
        form = LabPIForm()
    return render(request, 'portal/admin/create_lab.html',{'form':form})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def manage_users(request):
    search = request.GET.get('search','')
    users = User.objects.all()
    if search != '':
        fields = ['username__icontains','first_name__icontains','last_name__icontains','email__icontains']
        queries = [Q(**{field:search}) for field in fields]
        users = users.filter(reduce(operator.or_,queries))
    page_number = request.GET.get('page',1)
    per_page = request.GET.get('per_page',10)
    paginator = Paginator(users,per_page)
    page = paginator.page(page_number)
    return render(request, 'portal/admin/manage_users.html',{'paginator':paginator,'page':page,'search':search})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def user_details(request,username):
    user = User.objects.get(username=username)
    return render(request, 'portal/user_details.html',{'u':user})

@login_required
@user_passes_test(lambda u: u.is_superuser)
def modify_principal(request,username):
    user = User.objects.get(username=username)
    if request.method == 'GET':
        form = PrincipalForm(principal=user.principal)
    elif request.method == 'POST':
        form = PrincipalForm(request.POST,principal=user.principal)
        if form.is_valid():
            form.save()
            return redirect('user_details',username=user.username)
    return render(request, 'portal/user_details.html',{'u':user,'form':form})

