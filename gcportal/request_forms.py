import sys, inspect
from django import forms
from gcportal.emails import Mailer
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import InlineCheckboxes
from django.conf import settings

class RoundupForm(forms.Form):
    def __init__(self,*args,**kwargs):
        self.user = kwargs.pop('user',None)
        super(RoundupForm, self).__init__(*args,**kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
    to_email = settings.REQUEST_TO_EMAIL
    subject = 'Helpdesk'
    success_message = 'Your request has been submitted.'
    keywords = []
    def get_keywords(self):
        if len(self.keywords)>0:
            return ' [keyword=%s]' % ','.join(self.keywords)
        return ''
    def get_subject(self):
        return '%s%s' % (self.subject,self.get_keywords())
    def get_success_message(self):
        return self.success_message
    def process(self):
        Mailer().send_mail(self.get_subject(), to=[self.to_email],from_email=self.user.email,bcc=[], context={'form':self}, message_template='portal/emails/request_form_body.txt')
        return self.get_success_message()

class TicketForm(RoundupForm):
    label = 'Submit a ticket'
    subject = forms.CharField()
    job_id = forms.IntegerField()
    server = forms.CharField()
    command = forms.CharField()
    directory = forms.CharField()
    description = forms.CharField(widget=forms.Textarea)
    def get_subject(self):
        return '%s%s' % (self.cleaned_data['subject'],self.get_keywords())

class SoftwareForm(RoundupForm):
    def __init__(self,*args,**kwargs):
        super(SoftwareForm, self).__init__(*args,**kwargs)
        self.helper[4] = InlineCheckboxes('tags')
    TAG_CHOICES = ('modules','modules'), ('data analysis','data analysis'), ('libraries','libraries'), ('aligner','aligner'), ('assembler','assembler'), ('cluster','cluster'), (' databases',' databases'), ('patterns','patterns')
    label = 'Request software'
    subject = 'Software request'
    keywords = ['software']
    name = forms.CharField()
    version = forms.CharField()
    link = forms.URLField()
    description = forms.CharField(widget=forms.Textarea)
    tags = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,choices=TAG_CHOICES)
#     Layout(
#     Div(
#         'field1',
#         'field2',
#         template='my_div_template.html'
#     )
# )
    def process(self):
        Mailer().send_mail(self.get_subject(), to=[self.to_email],from_email=self.user.email,bcc=[], context={'form':self,'user':self.user}, message_template='portal/emails/software_request.txt')
        return self.get_success_message()
# forms_dict = dict([(name, cls) for name, cls in request_forms.__dict__.items() if isinstance(cls, forms.Form)])
#     return render(request, 'portal/request_forms.html',{'forms':forms_dict})


request_forms = {'TicketForm':TicketForm, 'SoftwareForm':SoftwareForm}