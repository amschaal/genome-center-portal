from django.conf import settings
from django.contrib.auth.models import User
from django.core import validators
from django.core.mail import send_mail
from django.db import models
from django.template.context import Context
from django.template.loader import get_template
from gcportal.emails import Mailer
import subprocess
import datetime
from django_confirm.models import Token
from django.urls.base import reverse
from kerberos.admin import KerberosAdmin, Principal
from signals import revoke_sponsor, set_sponsor
from django_logger.models import Log

class Lab(models.Model):
    first_name = models.CharField(max_length=30,null=True,blank=True,db_index=True)
    last_name = models.CharField(max_length=50, unique=True)
    description = models.TextField(blank=True,null=True)
#     owner = models.ForeignKey(User)
    @property
    def name(self):
        return '%s, %s'%(self.last_name,self.first_name) if self.first_name else self.last_name
    def has_permission(self,user,permission,use_superuser=True):
        if permission == Log.PERMISSION_VIEW:
            permission = LabPermission.PERMISSION_ADMIN
        return (use_superuser and user.is_superuser) or LabPermission.objects.filter(lab=self,user=user,permission=permission).first() is not None 
    def __unicode__(self):
        return self.name

# class Lab(models.Model):
#     first_name = models.CharField(max_length=30,null=True,blank=True,db_index=True)
#     last_name = models.CharField(max_length=30,db_index=True)
#     affiliation = models.CharField(max_length=50,choices=((affiliation,affiliation) for affiliation in settings.LAB_AFFILIATIONS),null=True,blank=True,db_index=True)
#     description = models.TextField(db_index=True)
#     url = models.URLField(blank=True,null=True)
#     slug = models.SlugField(max_length=50,unique=True,null=True)
#     @property
#     def name(self):
#         return '%s, %s'%(self.last_name,self.first_name) if self.first_name else self.last_name
#     def get_directory_name(self):
#         return call_directory_function('get_lab_directory_name',self)
# #         parts = [self.last_name,self.first_name] if self.first_name else [self.last_name]
# #         return make_directory_name('_'.join(parts))
#     def get_group_directory(self,group,full=True):
#         return call_directory_function('get_group_lab_directory',self,group,full=full)
# #         path = os.path.join(make_directory_name(group.name),'labs',self.get_directory_name())#self.slug
# #         if full:
# #             path = safe_join(FILES_ROOT,path)
# #         return path
#     def __unicode__(self):
#         return self.name

class LabPermission(models.Model):
#     PERMISSION_MEMBER = 'member'
    PERMISSION_APPROVER = 'approver'
    PERMISSION_ADMIN = 'admin'
    GROUP_PERMISSIONS = ((PERMISSION_APPROVER,"Can approve group members"),(PERMISSION_ADMIN,"Can administer group"))#(PERMISSION_MEMBER,"Group member"),
    user = models.ForeignKey(User,related_name='lab_permissions')
    lab = models.ForeignKey(Lab,related_name='lab_permissions')
    permission = models.CharField(choices=GROUP_PERMISSIONS,max_length=15)
    @staticmethod
    def get_labs_with_permissions(user,permissions,qs=None,require_all=True,use_superuser=True):
        qs = qs if qs else Lab.objects.all()
        if use_superuser and user.is_superuser:
            return qs
        qs = qs.filter(lab_permissions__user=user)
        if not require_all:
            return qs.filter(lab_permissions__permission__in=permissions)
        for p in permissions:
            qs = qs.filter(lab_permissions__permission=p)
        return qs
    @staticmethod
    def has_permission(lab_id,user,permission,use_superuser=True):
        return (use_superuser and user.is_superuser) or LabPermission.objects.filter(lab_id=lab_id,user=user,permission=permission).first() is not None 
    class Meta:
        unique_together = ('user','lab','permission')

class UserProfile(models.Model):
    user = models.OneToOneField(User,related_name="profile")
    sponsor = models.ForeignKey(Lab, null=True, blank=True,on_delete=models.SET_NULL,help_text='Enter group or lab sponsoring the account.  If your group is not listed, please email "sysadmin@genomecenter.ucdavis.edu".',related_name='user_profiles')
    category = models.CharField(max_length=15, null=True, blank=True, choices=(('undergrad','Undergraduate Student'),('grad','Graduate Student'),('postdoc','Post Doctorate'),('staff','Staff'),('faculty','Faculty'),('other','Other')))
    ucd = models.BooleanField('UCD',default=False,help_text="Are you a UC Davis student or employee?")
    uid = models.PositiveIntegerField(unique=True,null=True,blank=True)
    def __unicode__(self):
        return str(self.user)
    def revoke_sponsor(self,revoked_by):
        Mailer().send_mail('Genome Center account sponsorship revoked', to=[self.user.email],bcc=[settings.SYSADMIN_TO_EMAIL], context={'revoked_by':revoked_by, 'user':self.user}, message_template='portal/emails/sponsorship_revoked.txt')
        sponsor = self.sponsor
        self.sponsor = None
        self.save()
        revoke_sponsor.send(sender=self.__class__,user=self.user,sponsor=sponsor)
#         self.expire_principal()
    def set_sponsor(self,lab):
        self.sponsor = lab
        self.save()
        set_sponsor.send(sender=self.__class__,user=self.user,sponsor=lab)
#         self.enable_principal()
    def get_expiration_date(self):
        if self.category in ['staff','faculty']:
            return None
        return datetime.datetime.now() + datetime.timedelta(5*365) #expires in 5 years for all others
#     def get_principal(self):
#         if not getattr(settings,'KRB_ENABLE',False):
#             return None
#         kadm = KerberosAdmin.create()
#         return kadm.get_principal(self.user)
#     def expire_principal(self):
#         if getattr(settings,'KRB_ENABLE',False):
#             kadm = KerberosAdmin.create()
#             kadm.set_expiration(self.user)
#     def enable_principal(self):
#         if getattr(settings,'KRB_ENABLE',False):
#             kadm = KerberosAdmin.create()
#             kadm.set_expiration(self.user,date=self.get_expiration_date())
    @staticmethod
    def generate_uid():
        profile = UserProfile.objects.filter(uid__isnull=False).order_by('-uid').first()
        print profile
        print profile.uid
        uid = profile.uid
        while True:
            uid += 1
            if subprocess.call(['getent','passwd',str(uid)]) == 2:
                return uid
    def create_email_change_token(self,email):
        expires = datetime.datetime.now() + datetime.timedelta(days=1)
        Token.objects.filter(action='change_email',object_id=self.user_id).delete()
        return Token.create(action='change_email',content_object=self.user,data={'email':email},expires=expires)
    def send_email_change(self,email):
        token = self.create_email_change_token(email)
        url = settings.BASE_URL+reverse('confirm_change_email', kwargs={'token':token.token})
        emails = [token.data['email']]
        Mailer().send_mail('Confirm email address change', to=emails, context={'url':url,'user':self.user}, message_template='portal/emails/change_email_body.txt')
    def create_password_reset_token(self):
        expires = datetime.datetime.now() + datetime.timedelta(days=1)
        Token.objects.filter(action='password_reset',object_id=self.user_id).delete()
        return Token.create(action='password_reset',content_object=self.user,expires=expires)
    def send_password_reset(self):
        token = self.create_password_reset_token()
        url = settings.BASE_URL+reverse('password_reset', kwargs={'token':token.token})
        Mailer().send_mail('Reset your Genome Center password', to=[self.user.email], context={'user':self.user,'url':url}, message_template='portal/emails/password_reset.txt')

def user_unicode(self):
    return "%s %s" % (self.first_name, self.last_name) if self.first_name and self.last_name else self.username
User.__unicode__ = user_unicode

def principal(self):
    if getattr(settings,'KRB_ENABLE',False):
        if not hasattr(self, '_principal'):
#             kadm = KerberosAdmin.create()
            self._principal = Principal(self.username)
        return self._principal
    else:
        return None
User.principal = property(principal)

def has_permission(self,user,permission):
    return user == self or user.is_superuser


class Policy(models.Model):
#     TYPE_NETWORK = 'network'
#     TYPE_OTHER = 'other'
#     type = models.CharField(max_length=15,choices=((TYPE_NETWORK,'Network use policy'),(TYPE_OTHER,'Other')))
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=100)
    text = models.TextField()
    required = models.BooleanField(default=True)
    active = models.BooleanField(default=True)
    class Meta:
        verbose_name_plural = "policies"

class AcceptedPolicy(models.Model):
    policy = models.ForeignKey(Policy,related_name='accepted_policies')
    user = models.ForeignKey(User,related_name='accepted_policies')
    text = models.TextField()
    accepted = models.DateTimeField(auto_now_add=True)
    class Meta:
        unique_together = ('policy','user')

# Probably going to use captive portal software for mac registration
class Machine(models.Model):
    TYPE_LAPTOP = 'laptop'
    TYPE_DESKTOP = 'desktop'
    TYPE_TABLET = 'tablet'
    TYPE_PHONE = 'phone'
    TYPE_SERVER = 'server'
    TYPE_OTHER = 'other'
    TYPES = ((TYPE_DESKTOP,'Desktop'),(TYPE_LAPTOP,'Laptop'),(TYPE_PHONE,'Phone'),(TYPE_TABLET,'Tablet'),(TYPE_SERVER,'Server'),(TYPE_OTHER,'Other'))
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    type = models.CharField(max_length=10,choices=TYPES)
#     operating_system = models.CharField()
    mac_address = models.CharField(max_length=20,
        validators=[
            validators.RegexValidator(r'^([0-9a-fA-F]{2}([:-]?|$)){6}$','Please enter a valid Mac address.', 'invalid'),
        ])
    mac_address_2 = models.CharField(max_length=20,
        validators=[
            validators.RegexValidator(r'^([0-9a-fA-F]{2}([:-]?|$)){6}$','Please enter a valid Mac address.', 'invalid'),
        ],null=True,blank=True)
    mac_address_3 = models.CharField(max_length=20,
        validators=[
            validators.RegexValidator(r'^([0-9a-fA-F]{2}([:-]?|$)){6}$','Please enter a valid Mac address.', 'invalid'),
        ],null=True,blank=True)
    mac_address_4 = models.CharField(max_length=20,
        validators=[
            validators.RegexValidator(r'^([0-9a-fA-F]{2}([:-]?|$)){6}$','Please enter a valid Mac address.', 'invalid'),
        ],null=True,blank=True)
    description = models.TextField()
    retired = models.DateTimeField(null=True,blank=True)
    entered = models.DateTimeField(null=True,blank=True)
    entered_by = models.ForeignKey(User,null=True,blank=True,related_name="+")
    def email_request(self):
        Mailer().send_mail('New machine registration requested', to=[settings.SYSADMIN_TO_EMAIL], context={'machine':self}, message_template='portal/emails/machine_registration.txt')
#         message = get_template('portal/emails/machine_registration.txt').render(Context({'machine':self}))
#         send_mail('New machine registration requested',message,settings.SYSADMIN_EMAIL,[settings.SYSADMIN_EMAIL])
    def email_removal_request(self):
        Mailer().send_mail('Machine removal requested', to=[settings.SYSADMIN_TO_EMAIL], context={'machine':self}, message_template='portal/emails/machine_removal.txt')
    def send_approval_email(self):
        if self.user.email:
            Mailer().send_mail('Genome Center mac address registration approved', to=[self.user.email], context={'machine':self}, message_template='portal/emails/machine_approved.txt')

# Group._meta.permissions += (GROUP_PERMISSIONS)

class UnixGroup(models.Model):
#     gid = models.IntegerField(unique=True)
    name = models.CharField(max_length=32,unique=True)
    description = models.TextField(null=True,blank=True)
    lab = models.ForeignKey(Lab,null=True,blank=True)
    user = models.ForeignKey(User,null=True,blank=True)
    def __unicode__(self):
        return self.name
    def users(self):
        output = subprocess.check_output(['getent','group',self.name])
        users = output.strip().split(':')[3].split(',')
        return users
    def user_queryset(self):
        return User.objects.filter(username__in=self.users())

# class UserGroup(models):
#     user = models.ForeignKey(User)
#     group = models.ForeignKey()
    

class SoftwareRequest(models.Model):
    user = models.ForeignKey(User)
    