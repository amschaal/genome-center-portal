from __future__ import unicode_literals

from django.apps import AppConfig
from django.conf import settings

class GCPortalConfig(AppConfig):
    name = 'gcportal'
    def ready(self):
        from signals import handlers
        if getattr(settings,'KRB_ENABLE',False):
            from kerberos import handlers
        