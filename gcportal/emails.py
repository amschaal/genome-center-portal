from django.template.loader import get_template
from django.template.context import Context
from django.conf import settings
from django.core.mail.message import EmailMultiAlternatives
from django.core import mail

class Mailer(object):
    def __init__(self):
        self.messages = []
    def get_base_context(self):
        return {'BASE_URL':settings.BASE_URL}
    def render_template(self,template,context):
        full_context = self.get_base_context()
        full_context.update(context)
        return get_template(template).render(Context(full_context))
    def send(self,fail_silently=False):
        with mail.get_connection(fail_silently):
            for msg in self.messages:
                msg.send()
    def send_mail(self,subject,from_email=settings.SYSTEM_FROM_EMAIL,reply_to=None,to=[],cc=None,bcc=None,context={},message=None,
                  html_message=None,message_template=None,html_message_template=None,fail_silently=False):
        self.create_mail(subject, from_email, reply_to, to, cc, bcc, context, message, html_message, message_template, html_message_template, fail_silently)
        self.send(fail_silently)    
    def create_mail(self,subject,from_email,reply_to=None,to=[],cc=None,bcc=None,context={},message=None,
                  html_message=None,message_template=None,html_message_template=None,fail_silently=False):
        message = message if message else self.render_template(message_template, context) if message_template else None
        html_message = html_message if html_message else self.render_template(html_message_template, context) if html_message_template else None
        msg = EmailMultiAlternatives(subject,message,from_email,to=to,bcc=bcc,cc=cc,reply_to=reply_to)
        if html_message:
            msg.attach_alternative(html_message, "text/html")
#         msg.send(fail_silently=fail_silently)
        self.messages.append(msg)
