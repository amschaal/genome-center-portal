from django import forms
from gcportal.models import Machine , Lab
from django.contrib.auth.models import User
from django.conf import settings
from kerberos.admin import KerberosAdmin



class MachineForm(forms.ModelForm):
    class Meta:
        model = Machine
        fields = ('type','description','mac_address','mac_address_2','mac_address_3','mac_address_4')

class PolicyForm(forms.Form):
    accept = forms.BooleanField(initial=False,required=True,error_messages={'required': "Acceptance of this policy is required",})

class RequestSponsorForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    lab = forms.ModelChoiceField(queryset=Lab.objects.all())
    def clean(self):
        cleaned_data = super(RequestSponsorForm, self).clean()
        email = cleaned_data.get('email')
        username = cleaned_data.get('username')
        user = User.objects.filter(email=email,username=username,profile__sponsor__isnull=True).first()
        if not user:
            raise forms.ValidationError("That email/username combination does not match any user requiring a sponsor")

class RequestPasswordForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    def clean(self):
        cleaned_data = super(RequestPasswordForm, self).clean()
        email = cleaned_data.get('email')
        username = cleaned_data.get('username')
        user = User.objects.filter(email=email,username=username,profile__sponsor__isnull=False).first()
        if not user:
            raise forms.ValidationError("That email/username combination couldn't be found")

class LabForm(forms.ModelForm):
    class Meta:
        model = Lab
        exclude = ()
        labels = {'last_name':'Last name / Company'}
#     def clean_email(self):


class LabPIForm(LabForm):
    user = None
    principal_exists = False
    email = forms.EmailField(label='PI Email',required=False,help_text="Optionally enter the email address of the PI for the group to assign them admin privileges.")
    username = forms.CharField(label='PI Username/Principal',required=False,help_text="If the PI already has a kerberos account, enter the principal.")
#     create = forms.BooleanField(label='Create account',default=False,help_text="Create the account if it doesn't yet exist.")
#     def clean_email(self):
    def clean(self):
        cleaned_data = super(LabPIForm, self).clean()
        email = cleaned_data.get('email')
        username = cleaned_data.get('username')
#         create = cleaned_data.get('create')
        if username:
            if not email:
                raise forms.ValidationError("You must provide an email when assigning/creating a PI for a lab")
            self.user = User.objects.filter(username=username).first()
            if self.user and self.user.email != email:
                raise forms.ValidationError("That email/username combination does not match any user")
            elif not self.user:
                if getattr(settings, 'KRB_ENABLE',False):
                    kadm = KerberosAdmin.create()
                    if not kadm.username_exists(username):
                        raise forms.ValidationError("That kerberos principal does not exist")
                    else:
                        self.principal_exists = True
                else:
                    raise forms.ValidationError("No user with that username exist")
                    
        else:
            self.user = User.objects.filter(email=email).first()
        
class PrincipalForm(forms.Form):
    name = 'Modify Principal'
    expire = forms.DateTimeField(label="Account expiration date",required=False)
    pwexpire = forms.DateTimeField(label="Password expiration date",required=False)
    policy = forms.CharField(label="Password policy",required=False)
    def __init__(self,*args,**kwargs):
        self.principal = kwargs.pop('principal')
        if len(args) == 0 and not kwargs.get('data',None):
            kwargs['initial'] = self.principal.serialize()#{name:getattr(self.principal, name, None) for name, field in self.fields.iteritems()}#{'expire':self.principal.expire,'pwexpires':self.principal.pwexpires,'policy':self.policy}
        super(PrincipalForm, self).__init__(*args,**kwargs)
    def save(self):
        if self.is_valid():
            for key,val in self.cleaned_data.iteritems():
                setattr(self.principal.princ, key, val or None)
            self.principal.princ.commit()
            
                
