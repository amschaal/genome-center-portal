from django import template
import markdown
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter
@stringfilter
def markup(text):
    """Removes all values of arg from the given string"""
    return mark_safe(markdown.markdown(text))