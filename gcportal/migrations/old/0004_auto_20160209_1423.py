# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('gcportal', '0003_auto_20160209_1158'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='policy',
            options={'verbose_name_plural': 'policies'},
        ),
        migrations.AlterField(
            model_name='acceptedpolicy',
            name='policy',
            field=models.ForeignKey(related_name='accepted_policies', to='gcportal.Policy'),
        ),
        migrations.AlterField(
            model_name='acceptedpolicy',
            name='user',
            field=models.ForeignKey(related_name='accepted_policies', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='acceptedpolicy',
            unique_together=set([('policy', 'user')]),
        ),
    ]
