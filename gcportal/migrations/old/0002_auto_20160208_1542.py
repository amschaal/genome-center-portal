# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gcportal', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SoftwareRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='machine',
            name='mac_address',
            field=models.CharField(unique=True, max_length=20, validators=[django.core.validators.RegexValidator(b'^([0-9a-fA-F]{2}([:-]?|$)){6}$', b'Please enter a valid Mac address.', b'invalid')]),
        ),
    ]
