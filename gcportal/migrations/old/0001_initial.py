# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Machine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('type', models.CharField(max_length=10, choices=[(b'desktop', b'Desktop'), (b'laptop', b'Laptop'), (b'phone', b'Phone'), (b'tablet', b'Tablet'), (b'server', b'Server'), (b'other', b'Other')])),
                ('mac_address', models.CharField(max_length=20)),
                ('description', models.TextField()),
                ('retired', models.BooleanField(default=False)),
                ('entered', models.DateTimeField(null=True, blank=True)),
                ('entered_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
