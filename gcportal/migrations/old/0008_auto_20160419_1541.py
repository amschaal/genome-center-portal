# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('gcportal', '0007_auto_20160223_1652'),
    ]

    operations = [
        migrations.AddField(
            model_name='machine',
            name='mac_address_2',
            field=models.CharField(max_length=20, null=True, validators=[django.core.validators.RegexValidator(b'^([0-9a-fA-F]{2}([:-]?|$)){6}$', b'Please enter a valid Mac address.', b'invalid')]),
        ),
        migrations.AddField(
            model_name='machine',
            name='mac_address_3',
            field=models.CharField(max_length=20, null=True, validators=[django.core.validators.RegexValidator(b'^([0-9a-fA-F]{2}([:-]?|$)){6}$', b'Please enter a valid Mac address.', b'invalid')]),
        ),
        migrations.AddField(
            model_name='machine',
            name='mac_address_4',
            field=models.CharField(max_length=20, null=True, validators=[django.core.validators.RegexValidator(b'^([0-9a-fA-F]{2}([:-]?|$)){6}$', b'Please enter a valid Mac address.', b'invalid')]),
        ),
        migrations.AlterField(
            model_name='grouppermission',
            name='group',
            field=models.ForeignKey(related_name='group_permissions', to='auth.Group'),
        ),
        migrations.AlterField(
            model_name='grouppermission',
            name='user',
            field=models.ForeignKey(related_name='group_permissions', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='machine',
            name='mac_address',
            field=models.CharField(max_length=20, validators=[django.core.validators.RegexValidator(b'^([0-9a-fA-F]{2}([:-]?|$)){6}$', b'Please enter a valid Mac address.', b'invalid')]),
        ),
    ]
