# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('gcportal', '0008_auto_20160419_1541'),
    ]

    operations = [
        migrations.AlterField(
            model_name='machine',
            name='mac_address_2',
            field=models.CharField(blank=True, max_length=20, null=True, validators=[django.core.validators.RegexValidator(b'^([0-9a-fA-F]{2}([:-]?|$)){6}$', b'Please enter a valid Mac address.', b'invalid')]),
        ),
        migrations.AlterField(
            model_name='machine',
            name='mac_address_3',
            field=models.CharField(blank=True, max_length=20, null=True, validators=[django.core.validators.RegexValidator(b'^([0-9a-fA-F]{2}([:-]?|$)){6}$', b'Please enter a valid Mac address.', b'invalid')]),
        ),
        migrations.AlterField(
            model_name='machine',
            name='mac_address_4',
            field=models.CharField(blank=True, max_length=20, null=True, validators=[django.core.validators.RegexValidator(b'^([0-9a-fA-F]{2}([:-]?|$)){6}$', b'Please enter a valid Mac address.', b'invalid')]),
        ),
    ]
