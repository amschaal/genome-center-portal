# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-21 20:04
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gcportal', '0012_remove_lab_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='unixgroup',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='uid',
            field=models.PositiveIntegerField(blank=True, null=True, unique=True),
        ),
    ]
