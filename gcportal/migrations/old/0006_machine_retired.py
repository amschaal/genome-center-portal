# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gcportal', '0005_remove_machine_retired'),
    ]

    operations = [
        migrations.AddField(
            model_name='machine',
            name='retired',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
