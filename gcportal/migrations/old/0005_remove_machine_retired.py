# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gcportal', '0004_auto_20160209_1423'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='machine',
            name='retired',
        ),
    ]
