# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-16 18:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gcportal', '0011_unixgroup'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lab',
            name='owner',
        ),
    ]
