DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gcportal',
        'USER': 'dev',
        'PASSWORD': 'dev',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

SYSTEM_FROM_EMAIL = 'no-reply@genomecenter.ucdavis.edu'
SYSADMIN_FROM_EMAIL = 'sysadmin@genomecenter.ucdavis.edu'
SYSADMIN_TO_EMAIL = 'amschaal@ucdavis.edu'

BASE_URL = 'http://127.0.0.1:8001'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
